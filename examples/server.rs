#![forbid(unsafe_code)]

extern crate bincode;
extern crate serde;
#[macro_use] extern crate serde_derive;

use bacon::{ deserialize, Bacon, ciphers::{ Authenticate, chacha20::ChaCha20, Cipher, Decrypt, Nonce } };
use bigint::uint::U256;
use std::{ net::UdpSocket };

#[derive(Debug, Deserialize, Serialize)]
struct Person { name: String }

fn authenticate(bacon: &Bacon) -> Option<Result<String, String>> {
    let key: U256 = U256::from_dec_str("102853573294759285723534561345875635123503952762319857163587163501983275012378").unwrap();
    let cipher = ChaCha20::new(key, Some(Nonce::BaconDefault));
    match &bacon.fat {
        Some(fat) => {
            match fat.descr.get("Tag") {
                Some(tag) => {
                    if tag == &cipher.hash(&bacon).to_string() {
                        Some( Ok("MAC Ok".to_string()) )
                    } else { Some( Err("Tag does not match hash of bacon.".to_string() ) )  }
                },
                None => { Some( Err("Tag missing. Cannot authenticate.".to_string() ) ) }
            }
        },
        None => { None } //("No description available. Cannot authenticate."
    }
}

fn handle_error<T>(e: T) where T: std::fmt::Debug { println!("ERROR: {:?}", e); }

/// run example: cargo run --example server 127.0.0.1:64100
fn main() -> std::io::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    let socket = UdpSocket::bind(&args[1])?;
    println!("Server up and running at {:?}", socket);
    let mut buf: [u8; 512] = [0; 512];
    loop {
        match &socket.recv_from(&mut buf) {
            Ok((amt, src)) => {
                println!("{} bytes received from {:?}", amt, src);
                let m = match bincode::deserialize::<Bacon>(&buf) {
                    // well-formed bacon received
                    Ok(mut bacon) => {
                        dbg!(&bacon);
                        if let Some(fat) = &bacon.fat {
                            match fat.descr.get("Type") {
                                Some(bacon_type) => {
                                    if bacon_type == "std::string::String" {
                                        let key: U256 = U256::from_dec_str("102853573294759285723534561345875635123503952762319857163587163501983275012378").unwrap();
                                        let cipher = ChaCha20::new(key, Some(Nonce::BaconDefault));
                                        bacon = cipher.decrypt(bacon);
                                        dbg!(deserialize!(bacon, String));
                                    } else {
                                        println!("Type not accepted."); 
                                    }
                                }, 
                                None =>{ println!("Type declaration missing. Cannot serialize bacon.") }
                            }
                        }
                    },
                    // invalid data
                    Err(e) => {
                        let mut msg = String::from(e.to_string());
                        msg.push_str("The request wasn't bacon conform!");
                        dbg!(&msg);
                    }
                };
            },
            Err(_e) => { unreachable!() }      
        }
    }
}