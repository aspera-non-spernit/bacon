#![forbid(unsafe_code)]

extern crate bacon;
extern crate serde;
#[macro_use] extern crate serde_derive;
use bacon::{ Bacon, BaconState, ciphers::{ Cipher, Encrypt, speck::Speck}, Fat }; // a generic wrapper 
use std::collections::HashMap;
#[derive(Debug, Deserialize, Serialize)]
pub struct Fryable { data: Vec<String> }

/// Takes a key and a number of messages, collects them in a Vec<String>
/// Displays, the passed messages, the serialized but unfried bacon,
/// and the fried bacon
/// $ cargo run --example cli {mandatory secret_key} ["messages"]
/// $ cargo run --example cli my_password "Message in a bottle" "Jeanny"
fn main() {
    // key from cli args
    let mut args: Vec<String> = std::env::args().collect();
    let mut key_u128 = bacon::utils::key_128(&args[1]);
    args.drain(0..2);  // that is the program name and secret
    dbg!(&args);
    let data: Vec<String> = args.iter()
        .map(|m| m)
        .cloned()
        .collect();

    let fryable = Fryable { data: data };
    let bacon = Bacon::new(
        Some(
            Fat {
                state: BaconState::Unfried,
                descr: HashMap::new()
            }
        ),
        fryable
    );
    dbg!(&bacon);
    let cipher: Speck = Speck::new(key_u128, None);
    println!("The encrypted messages");
    dbg!(cipher.encrypt(bacon));

}