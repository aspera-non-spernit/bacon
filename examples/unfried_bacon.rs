#![forbid(unsafe_code)]

#[macro_use] extern crate bacon;

use bacon::{ Bacon };
/// Example takes a string from the cli and
/// creates an unfried (unencrypted) Bacon struct (serialized, block sized [u8])
/// $ cargo run --example unfried_bacon ["messages"]
/// Example: cargo run --example unfried_bacon "Message in a Bottle." 
fn main() {
    // key from cli args
    let mut args: Vec<String> = std::env::args().collect();
    args.drain(0..1);                   
    dbg!(&args);                                   
    let bacon = Bacon::from(args.pop().unwrap().clone());
    dbg!(&bacon);
    let string = deserialize!(bacon, String);
    dbg!(string.unwrap());
}