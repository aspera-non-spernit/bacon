#![forbid(unsafe_code)]

#[macro_use] extern crate bacon;
extern crate bigint;
#[macro_use] extern crate serde_derive;
use bacon::{ Bacon, BaconState, ciphers::{ Cipher, chacha20::ChaCha20, Decrypt, Encrypt, Nonce } };
use bigint::uint::U256;
use rand::Rng;
use std::{
    collections::{ hash_map::DefaultHasher, HashMap },
    fs::{ File },
    io::{ BufReader, Read },
    hash::{Hash, Hasher},
    net::{ UdpSocket },
    path::Path
};

#[derive(Clone, Debug, Deserialize, Hash, Serialize)]
struct Package {
    item: usize,  // n of m
    data: Vec<u8>
}

#[derive(Debug)]
struct JobPool {
    jobs: Option<HashMap<String, Vec<Bacon>>>,
}

fn package(file: &str) -> std::io::Result<Vec<Package>> {
    let f = File::open(file)?;
    let mut reader = BufReader::new(f);
    let mut buf = [0_u8; 512];
    let mut packages = vec![];
    let mut pid: usize = 0;
    for _ in 0..buf.len() {
        reader.read(&mut buf);
        packages.push( Package { item: pid, data: buf.to_vec() } );
        pid += 1;
    }
    Ok(packages)
}

fn bind_addr(bind_ip: &str) -> String {
    let mut rng = rand::thread_rng();
    let mut socket_addr = bind_ip.to_string();
    socket_addr.push_str(":");
    socket_addr.push_str(&rng.gen_range(49152, 65535).to_string()); // Dynamic, private or ephermal port range
    dbg!(&socket_addr);
    socket_addr
}

fn client(socket: UdpSocket, file: &str, server_addr: &str) {
    let key: U256 = U256::from_dec_str("102853573294759285723534561345875635123503952762319857163587163501983275012378").unwrap();  
    let cipher: ChaCha20 = ChaCha20::new(key, Some(Nonce::BaconDefault));

    dbg!(&socket);
    let packages = package(file).unwrap();
    let mut descr: HashMap<String, String> = HashMap::new();
    descr.insert("file_name".to_string(), Path::new(file).file_name().unwrap().to_str().unwrap().to_string() );
    let mut hasher = DefaultHasher::new();
    file.hash(&mut hasher);
    let hash = hasher.finish().to_string();
    for p in &packages {
        descr.insert("Item".to_string(), p.item.to_string() );
        descr.insert("Size".to_string(), (packages.len() - 1).to_string() );  
        descr.insert("Hash".to_string(), hash.clone());
        let mut bacon = Bacon::new(BaconState::Unfried, Some(descr.clone()), p.clone());
        bacon = cipher.encrypt(bacon);
      
        let mut buf: Vec<u8> = vec![];
        buf = bincode::serialize(&bacon).unwrap();
        drop(bacon);
        match socket.send_to(&buf, server_addr) {
            Ok(size) => {
                println!("{:?} bytes sent to {:?}", size, server_addr);
            },
            Err(e) => {  println!("Cannot sent to {:?}: {:?}", server_addr, e); }
        }
        drop(buf);

    }
}
fn server(socket: UdpSocket) {
    println!("Server up and running at {:?}", socket);
    let key: U256 = U256::from_dec_str("102853573294759285723534561345875635123503952762319857163587163501983275012378").unwrap();  
    let mut job_pool = JobPool { jobs: None };

    let mut buf: [u8; 670] = [0; 670];
    loop {
        match socket.recv_from(&mut buf) {
            Ok((amt, src)) => {
                println!("{} bytes received from {:?}", amt, src);
                match bincode::deserialize(&buf) {
                    Ok(b) => {
                        let mut bacon: Bacon = b;
                        let cipher = ChaCha20::new(key, Some(Nonce::BaconDefault));
                        bacon = cipher.decrypt(bacon);
                        let descr = &mut bacon.descr.unwrap();
                        let hash = descr.entry("Hash".to_string()).or_default();
                        match &job_pool.jobs {
                            None => {
                                let mut jobs = HashMap::new();
                                let mut job = vec![];
                                job.push(bacon.clone());
                                jobs.insert(hash.to_string(), job); // insert first job
                                job_pool.jobs = Some(jobs);
                                println!("{:?} {:?}", hash, job_pool);
                            }, 
                            Some(jobs) => {
                                println!("{:?} {:?}", hash, &jobs.get(hash) );
                            }
                        }   
                    }
                    Err(e) => {
                        let mut msg = String::from(e.to_string());
                        msg.push_str("The request wasn't bacon conform!");
                        dbg!(&msg);
                    }
                }
            },
            Err(_e) => { unreachable!() }
        }
    }
}
// server: $ bcp server 127.0.0.1:64100
// client: $ bcp 127.0.0.1 /home/user/Music/lalala.mp3 127.0.0.1:64100
fn main() -> std::io::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    if &args[1] == "server" {
        server( UdpSocket::bind(&args[2])? );
    } else {
        client( UdpSocket::bind(&bind_addr("127.0.0.1"))?, &args[2], &args[3]);
    };
    Ok(())
}