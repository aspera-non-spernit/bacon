#![forbid(unsafe_code)]

#[macro_use] extern crate bacon;
extern crate bigint;
extern crate bincode;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate rand;
use bacon::{ Bacon, BaconState, ciphers::{ Authenticate, Cipher, chacha20::ChaCha20, Encrypt, Nonce }, Fat };
use bigint::uint::U256;
use std::{ collections::HashMap, io, net::{ UdpSocket, SocketAddr, ToSocketAddrs }, str::FromStr };
use rand::{ Rng };


#[derive(Debug, Deserialize, Serialize)]
struct Person { name: String }

fn await_response(socket: &UdpSocket) {
    loop {
        // the server returns a success bacon
        let mut recv_buf = [0; 512];
        match socket.recv_from(&mut recv_buf) {
            Ok((amt, src)) => {
                println!("{} bytes received from {}", amt, src);
                let bacon: Bacon = bincode::deserialize(&recv_buf).unwrap();
                dbg!(deserialize!(bacon, String));
                break;
            },
            _ => { /* waiting .. TODO: timeout  */ }
        }
    }
}

fn bind_addr(bind_ip: &str) -> String {
    let mut rng = rand::thread_rng();
    let mut socket_addr = bind_ip.to_string();
    socket_addr.push_str(":");
    socket_addr.push_str(&rng.gen_range(49152, 65535).to_string()); // Dynamic, private or ephermal port range
    socket_addr
}

fn send(send_buf: Vec<u8>, remote_addrs: &[String]) -> std::io::Result<()> {
    let socket = UdpSocket::bind(&bind_addr("127.0.0.1"))?;
    let addrs: Vec<SocketAddr> = remote_addrs.iter()
        .map(|b| SocketAddr::from_str(b).unwrap())
        .collect();
  
    for a in addrs {
        match socket.send_to(&send_buf, a) {
            Ok(size) => {
                println!("{:?} bytes sent to {:?    }", size, &a);
            },
            Err(e) => {  println!("Cannot sent to {:?}: {:?}", &a, e); }
        }
    }
    Ok(())
}

fn chat(cipher: ChaCha20, remote_addrs: Vec<String>) -> std::io::Result<()> {
    loop {
        let mut read_buf = String::new();
        io::stdin().read_line(&mut read_buf)?;
        if &read_buf == ":quit\n" {
            dbg!("Bye bye.");
            break;
        }
        // Creating an unfried bacon from entered message
        let mut descr = HashMap::new();
        // descr.insert("Tag".to_string(), "".to_string());
        // some more instructions for the server
        descr.insert("Cipher".to_string(), "bacon::ciphers::chacha20::ChaCha20".to_string());
        descr.insert("Type".to_string(), "std::string::String".to_string());
        let mut bacon = Bacon::new(
            Some(
                Fat{
                    state: BaconState::Unfried,
                    descr: descr
                }
            ),
            read_buf
        );
        bacon = cipher.encrypt(bacon); 

        let send_buf = bincode::serialize(&bacon).unwrap();
        drop(bacon);
        send(send_buf, &remote_addrs);
        // sending the bacon to the bacon server
        
        println!("Waiting for response..");

        // loop {
        //     // the server returns a success bacon
        //     let mut recv_buf = [0; 512];
        //     match socket.recv_from(&mut recv_buf) {
        //         Ok((amt, src)) => {
        //             println!("{} bytes received from {}", amt, src);
        //             let bacon: Bacon = bincode::deserialize(&recv_buf).unwrap();
        //             dbg!(deserialize!(bacon, String));
        //             break;
        //         },
        //         _ => {}
        //     }
        // }
    } 
    Ok(())
}

/// Requires example/server running
/// Opens a command line "chat". A message is sent to the server after ENTER hit.
/// Run: cargo run --example chat {local_ip} {remote_addr}
/// Example: cargo run --example chat 127.0.0.1 127.0.0.1:64100
fn main() -> std::io::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    let remote_addrs: Vec<String> = args.iter()
        .skip(2)
        .map(|addr| addr)
        .cloned()
        .collect();
    // key, cipher, socket and buffer used for all examples
    // The key between client and server must be shared through different channels
    // The ChaCha20 for this example is a default used on the server too.
    let key: U256 = U256::from_dec_str("102853573294759285723534561345875635123503952762319857163587163501983275012378").unwrap();  
    let cipher: ChaCha20 = ChaCha20::new(key, Some(Nonce::BaconDefault));
    chat(cipher, remote_addrs);
    Ok(())
}

/***

      // malicious bacon using wrong key or nonce
        // error message
/*
        let mut mal_bacon = Bacon::from("Malicious Bacon".to_string());
        let mal_cipher = ChaCha20::new(wrong_key, Some(Nonce::BaconDefault));
        mal_bacon = mal_cipher.encrypt(mal_bacon);
        let mal_mac = cipher.hash(&mal_bacon);
        let mut mal_descr = HashMap::new();
        mal_descr.insert("Tag".to_string(), mal_mac.to_string());
        mal_descr.insert("Cipher".to_string(), "bacon::ciphers::chacha20::ChaCha20".to_string());
        mal_descr.insert("Type".to_string(), "bacon::examples::String".to_string());
        bacon.descr = Some(mal_descr);
        buf = bincode::serialize(&mal_bacon).unwrap();
*/


// $ cargo run --example client 127.0.0.1:64100 string "Super secret message"
// EXAMPLE 1 : sending a string from the command line in an encrypted bacon
if &args[3] == "string" {
    // Bacon::new serializes the payload, but it's not yet encrypted
    // Note: description (aka Header): None 
    let mut bacon = Bacon::new(
        Some(
            Fat{
                state: BaconState::Unfried,
                descr: HashMap::new()
            }
        ),
        args[4].clone()
    );
    println!("The string message from the command line as serialized (fried) bacon.");
    dbg!(&bacon);
    // It can be deserialized with:
    let clone = bacon.clone();          // clone only for this example
    let original = deserialize!(clone, String);
    println!("The original message from a unfried bacon.");
    dbg!(&original);
    // Encrypt the bacon
    // Using a Nonce that is known on the server, without sharing the nonce over the net
    // There is no secure implementation yet to exchange keys securely.
    bacon = cipher.encrypt(bacon);
    // If the bacon should be stored, but not decrypted on the server
    // The bacon can be send as it is.BaconState
    // We do not include a description

    // Trying to let authentication fail (uncomment to test.)
    // (Error): No description available. Cannot authenticate. Probably not required 
    // Error: Tags do not match (Bacon hash does not match authentication tag)
    // Error: Tag is missing in the description. Cannot authenticate.
    // If authentication fails (hash) bacon will be returned.
    // In this example the bacon misses the auth tag and cannot be authenticated.
//  bacon = Bacon::from("This is a malicious bacon".to_string());
    // serialize bacon
    buf = bincode::serialize(&bacon).unwrap();
    drop(bacon);
    // sending the bacon to the bacon server
    match socket.send_to(&buf, &remote_addrs) {
        Ok(size) => {
            println!("{:?} bytes sent to {:?}", size, &remote_addrs);
        },
        Err(e) => { println!("Cannot sent to {:?}: {:?}", &remote_addrs, e);}
    }
    // Now we are waiting for the response. See loop { .. } at the end of main()
    // Waiting for the response
} 
// $ cargo run --example client 127.0.0.1:64100 person "Super secret message"
// EXAMPLE 2: Sendinng a struct Person to the server, including decryption instruction for the server
// else if &args[3] == "person" {
//     let p = Person { name: args[4].to_string() };
//     dbg!(&p);
//     let mut bacon = Bacon::new(
//         Some(
//             Fat{
//                 state: BaconState::Unfried,
//                 descr: HashMap::new()
//             }
//         ),
//         p
//     );
//     bacon = cipher.encrypt(bacon);
//     // We want the server to process the bacon. The server needs an authentication tag 
//     // and some info how to decrypt the bacon
//     // The nonce and key were "preshared" as both Server and Client use Nonce::BaconDefault.
//     // The tag can be retrieved from the cipher.hash function
//     let mac = cipher.hash(&bacon);
//     let mut descr = HashMap::new();
//     descr.insert("Tag".to_string(), mac.to_string());
//     // we also include some more instructions
//     descr.insert("Cipher".to_string(), "bacon::ciphers::chacha20::ChaCha20".to_string());
//     descr.insert("Type".to_string(), "bacon::examples::Person".to_string());
//     bacon.fat.descr = descr;
//     // bacon with tag ready to be shipped
//     println!("The encrypted bacon with authentication tag.");
//     dbg!(&bacon);
//     buf = bincode::serialize(&bacon).unwrap();
//     // sending the person to the bacon server
//     match socket.send_to(&buf, &remote_addrs) {
//         Ok(size) => {
//             println!("{:?} bytes sent to {:?}", size, &remote_addrs);
//         },
//         Err(e) => {  println!("Cannot sent to {:?}: {:?}", &remote_addrs, e); }
//     }
// $ cargo run --example chat 127.0.0.1 chat 
// EXAMPLE 3: Sendinng a chat message including decryption instruction for the server
//} 

***/