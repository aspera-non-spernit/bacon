#![forbid(unsafe_code)]

extern crate bacon;
extern crate serde;
extern crate serde_derive;
extern crate bigint;
extern crate bincode;
use bacon::{ Bacon, ciphers::{ Authenticate, Cipher, chacha20::ChaCha20, MAC, Nonce } };
use bigint::uint::U256;

/** 
    ChaCha20 can be used for simple Hashing.
*/

fn authenticate(hash: MAC) -> Result<String, String> {
    // hash password stored
    let cipher = ChaCha20::new(U256::from_dec_str("0").unwrap(), Some(Nonce::BaconDefault));
    let stored_pass_bacon = Bacon::from(String::from("super_secret_password"));
    let hash_pass = cipher.hash(&stored_pass_bacon);
    match hash_pass == hash {
        true => Ok(String::from("Password correct")),
        false => {
            Err(String::from("Password incorrect. HINT: The correct password is: super_secret_password"))
        }
    }
}
// example:
// $ cargo run --example hashing
fn main() {
    let args: Vec<String> = std::env::args().collect();
    match args.len() < 2 {
        true => panic!("Password required as first argument."),
        _ => {}
    }
    // user enters password for authentication
    let entered_password = args[1].clone();
    drop(args);
    // must be wrapped in a bacon
    let bacon_pass = Bacon::from(String::from(entered_password));
    // For hashing the encryption key is irrelevant, but must be passed since
    // Option not implemented yet, therefore 0
    let cipher = ChaCha20::new(U256::from_dec_str("0").unwrap(), Some(Nonce::BaconDefault));
    match authenticate(cipher.hash(&bacon_pass)) {
        Ok(s) => {dbg!(s); },
        Err(e) => {dbg!(e); }
    }
}