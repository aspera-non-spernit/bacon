#[forbid(unsafe_code)]
/**
    Decrypt the en
*/
extern crate bacon;
extern crate serde;
#[macro_use] extern crate serde_derive;
use bacon::{
    Bacon, deserialize, BaconState, ciphers::{ Cipher, Decrypt, speck::Speck}, Fat,
    utils
};
use std::{ collections::HashMap };

// $ cargo run --example challenge {16-digit-key}
// Example: cargo run --example challenge u.ijd.3HH8$n.MhK

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let key_u128 = utils::key_128(&args[1]);
    // Intercepted complete packet (sort of)
    // Decrypted and serialized the block represents a Vec<String>
    let packet: Vec<u128> = vec![
        194994240177673170410405868973883980079,
        117503074423148199102782414195769253755,
        219785934211444555000606164007802970751,
        12612389679069308565827184940017487097,
        220145133199359224886399099426334819388,
        299964722123935284235072136009735422054,
        174417525341693231544104851026224630320,
        329963556785934945585845274474866337862,
        226691656566171050711180956958335192736,
        88946727837051004332568529112028099160,
        29894476423457691686824500893903312287,
        147222905770499705299793989200652289440,
        217759079837809311343747195548587713750,
    ];
    dbg!(&packet);
    // DO NOT USE new, it serializes data. packet is already serialized and encrypted    
    // let encr_bacon = Bacon::new(BaconState::Fried, None, packet);
    // instead:
    let encr_bacon = Bacon {
        fat: Some(
            Fat {
                state: BaconState::Fried,
                descr: HashMap::new()
            }
        ),
        data: packet
    };
    dbg!(&encr_bacon);
  
    // trying to decrypt packet with key provided 
    let cipher: Speck = Speck::new(key_u128, None);
    // If decryption was successful, we should be able to deserialize the bacon into
    // struct Fryable
    // If the decryption was not successful this will fail.
    // Note: Neither the Cipher nor Bacon has knowledge about the data blocks
    // Therefore no error message on an decryption attempt
    let decr_bacon = cipher.decrypt(encr_bacon);
    dbg!(&decr_bacon);
    match deserialize!(decr_bacon, Vec<String>) {
        Ok(messages) => { dbg!(messages);},
        Err(e) => { dbg!(e);}
    }
}
