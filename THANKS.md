# THANKS

## bigint - v4.4.1

#### Repo:

- [https://github.com/ethcore/bigint](https://github.com/ethcore/bigint)

#### Authors:

- [Parity Technologies <admin@parity.io>](mailto:admin@parity.io)

## bincode - v1.1.2

#### Repo: 

- [https://github.com/TyOverby/bincode](https://github.com/TyOverby/bincode)

#### Docs:

- [https://docs.rs/bincode](https://docs.rs/bincode)

#### Authors: 

- [Ty Overby <ty@pre-alpha.com>](mailto:ty@pre-alpha.com)
- [Francesco Mazzoli <f@mazzo.li>](mailto:f@mazzo.li)
- [David Tolnay <dtolnay@gmail.com>](mailto:dtolnay@gmail.com)
- Daniel Griffen

## chacha - v0.3.0

#### Repo:

- [https://github.com/PeterReid/chacha](https://github.com/PeterReid/chacha)

#### Docs:

- [http://peterreid.github.io/chacha](http://peterreid.github.io/chacha)

#### Authors: 

- [Peter Reid <peter.d.reid@gmail.com>](peter.d.reid@gmail.com)


## rand - v0.6.5

#### Repo:

- [https://github.com/rust-random/rand](https://github.com/rust-random/rand)

#### Docs:

- [https://rust-random.github.io/rand](https://rust-random.github.io/rand)

#### Authors: 

- The Rand Project Developers
- The Rust Project Developers


## serde - v1.0.89

#### Repo:

- [https://github.com/serde-rs/serde](https://github.com/serde-rs/serde)

#### Docs:

- [https://docs.serde.rs/serde/](https://docs.serde.rs/serde/

#### Authors: 

- [Erick Tryzelaar <erick.tryzelaar@gmail.com>](mailto:erick.tryzelaar@gmail.com)
- [David Tolnay <dtolnay@gmail.com>](mailto:dtolnay@gmail.com)


##  serde_derive - v1.0.89

#### Repo:

- [https://github.com/serde-rs/serde](https://github.com/serde-rs/serde)

#### Docs:

- [https://serde.rs/derive.html](https://github.com/serde-rs/serde)

#### Authors: 

- [Erick Tryzelaar <erick.tryzelaar@gmail.com>](mailto:erick.tryzelaar@gmail.com)
- [David Tolnay <dtolnay@gmail.com>](mailto:dtolnay@gmail.com)

## serde_json - v1.0.39

#### Repo:

- [https://github.com/serde-rs/serde](https://github.com/serde-rs/json)

#### Docs:

- [http://docs.serde.rs/serde_json](http://docs.serde.rs/serde_json/)

#### Authors: 

- [Erick Tryzelaar <erick.tryzelaar@gmail.com>](mailto:erick.tryzelaar@gmail.com)
- [David Tolnay <dtolnay@gmail.com>](mailto:dtolnay@gmail.com)


## thank - v0.2.0

#### Description:

Shows info about crates used in your projects so you know who to thank for them and where to do the thanking.

#### Repo:

- [https://github.com/brown121407/thank](https://github.com/brown121407/thank)

#### Homepage:

- [https://github.com/brown121407/thank](https://github.com/brown121407/thank)

#### Authors: 

- [Sergiu Marton <brown121407@gmail.com>](mailto:brown121407@gmail.com)
