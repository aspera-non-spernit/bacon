//! The bacon crate is an interface to ease the implementation of new Ciphers.
//! Bacon is an adapter to the Speck and Chacha20 ciphers and provides functionality 
//! to en- and decrypt an arbitrary struct```struct T where T: Serialize + Deserialize```

#![forbid(unsafe_code)]
extern crate bincode;
extern crate serde;
#[macro_use] extern crate serde_derive;

pub mod ciphers;
pub mod utils;
/**
    A bacon represents a serialized and fryable (encryptable) struct
*/
use serde::{ Deserialize, Serialize };
use std::{ collections::HashMap };

#[macro_export]
macro_rules! serialize {
    ($item:ident) => {
        {    // Using $item twice like this can cause an expression with side effects to be evaluated twice.
            let item = $item;
            let byte_doc = bincode::serialize(&item).unwrap();
            let chunks = byte_doc.chunks(16);
            let mut data: Vec<u128> = vec![];
            let mut x: [u8; 16] =  [0; 16];
            for chunk in chunks {
                let mut count = 0;
                for byte in chunk.clone() {
                    x[count] = *byte;
                    count += 1;
                }
                data.push( u128::from_le_bytes(x) );
            }
            data
        }
    }
}

#[macro_export]
macro_rules! deserialize {
    ($bacon:ident, $target:ty) => {
        {
            let mut decr_bytes: Vec<u8> = vec![];
            for block in $bacon.clone().data {
                for byte in u128::to_le_bytes(block).iter() {
                    decr_bytes.push(*byte);
                }    
            }  
            let decr: bincode::Result<$target> = bincode::deserialize(&decr_bytes);
            decr
        }
    }
}


#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Fat {
    pub state: BaconState,
    pub descr: HashMap<String, String>
}
/// Fried: Data stored in encrypted form. Unfried: The data is serialized but not encrypted.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum BaconState {
    /// The data stored in Bacon is encrypted (and previously serialized)
    Fried,
    Stateless,
    /// The data stored in Bacon is serialized not not encrypted
    Unfried
}
/// ```Bacon``` is a reusable wrapper for an arbitrarty serialized struct stored in the field ```data: Vec<u128>```
/// The optional description can be used to share information regarding the Bacon, that may be neccessary to
/// en-/decrypt a Bacon
/**
    The field descr: HashMap<String, String> can be used to add decryption information
    data: Vec<u128> either contains the serialized, but not encrypted data blocks or encrypted data blocks
*/
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Bacon { pub fat: Option<Fat>, pub data: Vec<u128> }

/// Deserializes a generic Bacon into a concrete type T where T: serde::Deserialze<'de>
/// Does not deserialize a stream of [u8] into a Bacon
pub trait BaconDeserialize { fn deserialize<T>(&self) -> bincode::Result<T> where for<'de> T: serde::Deserialize<'de>,; }

impl Bacon {
    /// Create a new Bacon with State Fried | Unfried and d being the type that hold the data
    /// of the wrapped struct. Bacon serializes ```d: T``` into blocks in a Vec<u128>
    /// explicitly drops(d)
    pub fn new<T: for <'de> Deserialize<'de> + Serialize>(fat: Option<Fat>, d: T) -> Bacon {
        let data = serialize!(d);
        Bacon { fat, data }
    }
}

impl From<String> for Bacon {
    fn from(string:  String) -> Self {
        let data = serialize!(string);
        let fat = Fat { state: BaconState::Unfried, descr: HashMap::new() };
        Bacon { fat: Some(fat), data: data }
    }
}

/// Deserializes a generic Bacon into a concrete Type T
/// unused deserialize! instead
impl BaconDeserialize for Bacon {
    fn deserialize<T>(&self) -> bincode::Result<T> where for<'de> T: Deserialize<'de> {
        let mut decr_bytes: Vec<u8> = vec![];
        for block in &self.data {
            for byte in u128::to_le_bytes(*block).iter() {
                decr_bytes.push(*byte);
            }    
        }  
        bincode::deserialize(&decr_bytes)
    }
}