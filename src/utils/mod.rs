trait Key { fn key<V, T>(value: V, key_type: T) -> T; }
/// Utility function to turn a ```&str``` into an u128. The max length considered is 16 characters.
pub fn key_128(pass: &str) -> u128 {
    let mut x:  [u8; 16] = [0; 16];
    for (count, byte) in pass.as_bytes().iter().enumerate() {
        x[count] = *byte;
    }
    u128::from_le_bytes(x)
}