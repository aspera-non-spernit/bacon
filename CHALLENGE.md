# Bacon Challenge

Speck has been developed by the NSA (National Security Agency) of the USA and has since publishing caused lots of noise, raised doubts. 

It has been called deeply flawed and backdoors may have been implemented.

I am not a Cryptologist and cannot evaluate the claims. Therefore I present this challenge. 

Show me that the Speck algorhithm or the implementation used by bacon can be cracked. 

1. Intercepted data package

```rust
data: [
    94994240177673170410405868973883980079,
    117503074423148199102782414195769253755,
    219785934211444555000606164007802970751,
    12612389679069308565827184940017487097,
    220145133199359224886399099426334819388,
    299964722123935284235072136009735422054,
    174417525341693231544104851026224630320,
    329963556785934945585845274474866337862,
    226691656566171050711180956958335192736,
    88946727837051004332568529112028099160,
    29894476423457691686824500893903312287,
    147222905770499705299793989200652289440,
    217759079837809311343747195548587713750,
]
}
```

2. The original package is complete and can be fully recovered. Unfried it deserializes into a
```Vec<String>```. ```examples/command_line``` calls it ```struct Fryable```

```rust
#[derive(Debug, Deserialize, Serialize)]
pub struct Fryable { data: Vec<String> }
```

3. The secret key

The secret key is obviously *"lost"*. The original key was a 16 digit string of a-z A-Z 0-9 with special characters you find on any keyboard, such as the Shift-1 to Shift-0 keys or ,.-;:_+#*'.

No language specific characters (ie. no ä è ß etc.) or invisble characters like, return newline, break characters are used for the key.

4. example/challenge.rs

The command line tool can be used to decrypt/unfry the bacon, if brute-forcing the key is your approach.

```rust
cargo run --example challenge {16-digit-alphanumeric-key}
```

5. The wrong key

If the provided key is wrong you will receive an error message:

```rust
[examples/challenge.rs:28] e = Io(
    Custom {
        kind: UnexpectedEof,
        error: StringError(
            ""
        )
    }
)
```

6. Successful?

You your attempt to decrypt the message was successful, the tool displays messages similar to 
these:

```rust
messages = [
    "Congratulations!"
    "You mastered the challenge!",
    "You are the greatest!"
]
```
Follow the instructions that are displayed instead of the above example.